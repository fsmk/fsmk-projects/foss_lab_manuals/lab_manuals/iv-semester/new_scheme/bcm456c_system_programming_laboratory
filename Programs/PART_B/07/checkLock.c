//checkLock.c
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#define REGION_SIZE 100
#define LOCK_SIZE 50

int main() {
    int fd;
    struct flock fl;

    // Open the file
    if ((fd = open("demo.txt", O_RDWR)) == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    // Set up lock information
    fl.l_type = F_WRLCK;  // Exclusive lock
    fl.l_whence = SEEK_END;
    fl.l_start = -REGION_SIZE;
    fl.l_len = REGION_SIZE;

    // Try to acquire the lock
    if (fcntl(fd, F_GETLK, &fl) == -1) {
        perror("fcntl");
        exit(EXIT_FAILURE);
    }

    // Check if the region is locked
    if (fl.l_type != F_UNLCK) {
        printf("Region is locked by PID %d\n", fl.l_pid);
    } else {
        // Lock the region
        fl.l_type = F_WRLCK;
        if (fcntl(fd, F_SETLK, &fl) == -1) {
            perror("fcntl");
            exit(EXIT_FAILURE);
        }

        printf("Region locked by the current process with PID %d\n", getpid());

        // Read the last 50 bytes
        char buffer[LOCK_SIZE + 1];
        if (lseek(fd, -LOCK_SIZE, SEEK_END) == -1) {
            perror("lseek");
            exit(EXIT_FAILURE);
        }
        ssize_t bytes_read = read(fd, buffer, LOCK_SIZE);
        if (bytes_read == -1) {
            perror("read");
            exit(EXIT_FAILURE);
        }
        buffer[bytes_read] = '\0';  // Null-terminate the string
        printf("Last 50 bytes: %s\n", buffer);

        // Unlock the region
        fl.l_type = F_UNLCK;
        if (fcntl(fd, F_SETLK, &fl) == -1) {
            perror("fcntl");
            exit(EXIT_FAILURE);
        }
        printf("Region unlocked\n");
    }

    // Close the file
    if (close(fd) == -1) {
        perror("close");
        exit(EXIT_FAILURE);
    }

    return 0;
}

