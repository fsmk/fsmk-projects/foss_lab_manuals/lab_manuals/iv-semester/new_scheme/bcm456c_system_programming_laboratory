#include <stdio.h>
#include <unistd.h>
#include <signal.h>

// Signal handler function for the SIGALRM signal
void alarm_handler(int signum) {
    printf("Alarm triggered!\n");
    // Set up the alarm again for periodic triggering
    alarm(5); // Trigger alarm every 5 seconds
}

int main() {
    // Set up the signal handler for SIGALRM
    signal(SIGALRM, alarm_handler);

    // Set up the initial alarm to trigger after 5 seconds
    alarm(5);

    // Keep the program running to receive signals
    while(1) {
        // Do nothing, waiting for signals
        sleep(1);
    }

    return 0;
}

