$ gcc -Wall zombieDemo.c -o zombieDemo.x

$ ./zombieDemo.x 
Parent process (PID: 251204) created a child process with PID: 251205.
Child process (PID: 251205) is running.

PID	PPID	STATE	Name
 251205  251204 Z+   [zombieDemo.x] <defunct>
 251206  251204 S+   sh -c ps -eo pid,ppid,stat,cmd | grep Z+
 251208  251206 S+   grep Z+

Here Zombie processes have their state mentioned as Z+
