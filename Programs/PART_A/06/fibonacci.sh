#!/bin/bash

# Function to calculate Fibonacci sequence up to n terms
fibonacci() {
    n=$1
    a=0
    b=1

    echo "Fibonacci sequence up to $n terms:"

    if [ $n -ge 1 ]; then
        echo -n "$a "
    fi

    if [ $n -ge 2 ]; then
        echo -n "$b "
    fi

    i=2
    while [ $i -lt $n ]; do
        c=$((a + b))
        echo -n "$c "
        a=$b
        b=$c
        i=$((i + 1))
    done

    echo "" # Print newline after the sequence
}

# Main script
read -p "Enter a number greater than 3: " num

if [ $num -le 3 ]; then
    echo "Error: The number must be greater than 3."
    exit 1
fi

fibonacci $num

