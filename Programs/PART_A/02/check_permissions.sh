#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: $0 <file1> <file2>"
    exit 1
fi

file1=$1
file2=$2

if [ ! -e "$file1" ]; then
    echo "File '$file1' does not exist."
    exit 1
fi

if [ ! -e "$file2" ]; then
    echo "File '$file2' does not exist."
    exit 1
fi

perm1=$(stat -c "%a" "$file1")
perm2=$(stat -c "%a" "$file2")

if [ "$perm1" = "$perm2" ]; then
    echo "Common permissions: $perm1"
else
    echo "$file1: $perm1"
    echo "$file2: $perm2"
fi
