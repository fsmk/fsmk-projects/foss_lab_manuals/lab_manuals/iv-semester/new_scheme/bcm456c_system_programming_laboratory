Hard Link

Every file on the Linux filesystem starts with a single hard link. The link is between the filename and the actual data stored on the filesystem. Creating an additional hard link to a file means a few different things. Let's discuss these.

Here you create a new filename pointing to the exact same data as the old filename. This means that the two filenames, though different, point to identical data.  Observe linkDemo.c and mylink.c have the same inode number and both there  link count has also been changed to 2. Files that are hard-linked together share the same inode number.

When changes are made to one filename, the other reflects those changes. The permissions, link count, ownership, timestamps, and file content are the exact same. If the original file is deleted, the data still exists under the secondary hard link. The data is only removed from your drive when all links to the data have been removed.

Output

//Compiling
$ gcc -Wall linkDemo.c -o link

$ ls
link  linkDemo.c


//Execution
$ ./link linkDemo.c mylink.c

1.Hard Link
2.Symbolic Link
Choose an option : 1
Hard link created: mylink.c -> linkDemo.c

$ ls -lih
total 24K
4982374 -rwxrwxr-x 1 putta putta 16K Mar 20 17:53 link
4982384 -rw-rw-r-- 2 putta putta 817 Mar 20 17:50 linkDemo.c
4982384 -rw-rw-r-- 2 putta putta 817 Mar 20 17:50 mylink.c

//observe linkDemo.c and mylink.c have the same inode number


Symbolic Link

Commonly referred to as symbolic links, soft links link together non-regular and regular files. They can also span multiple filesystems. By definition, a soft link is not a standard file, but a special file that points to an existing file.


$ ./link linkDemo.c mylink2.c

1.Hard Link
2.Symbolic Link
Choose an option : 2
Symbolic link created: mylink2.c_symlink -> linkDemo.c

$ ls -li
total 24
4982374 -rwxrwxr-x 1 putta putta 16288 Mar 20 17:53 link
4982384 -rw-rw-r-- 2 putta putta   817 Mar 20 17:50 linkDemo.c
4984340 lrwxrwxrwx 1 putta putta    10 Mar 20 23:20 mylink2.c_symlink -> linkDemo.c
4982384 -rw-rw-r-- 2 putta putta   817 Mar 20 17:50 mylink.c

//observe linkDemo.c and mylink.c have different inode numbers



A hard link always points a filename to data on a storage device.

A soft link always points a filename to another filename, which then points to information on a storage device.
