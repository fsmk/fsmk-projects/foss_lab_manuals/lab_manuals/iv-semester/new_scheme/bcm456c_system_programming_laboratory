#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

int main() 
{
    DIR *directory;
    struct dirent *entry;
    char dirName[40];

	printf("Enter the directory path that you want to display or just press enter for current directory: \n");
	fgets(dirName, sizeof(dirName), stdin);
	
    // Removing trailing newline character if present
	if(dirName[strlen(dirName) - 1] == '\n') 
	{
        dirName[strlen(dirName) - 1] = '\0';
    }

	
	//Check if only enter has been pressed
	if (dirName[0] == '\0' || dirName[0] == '\n') 
	{
		// Open the current directory
		directory = opendir(".");
	}
	else
	{
		directory = opendir(dirName);
	}
	
    if (directory == NULL) {
        perror("Unable to open directory");
        return 1;
    }

    // Read directory entries
    printf("Contents of the directory:\n");
    while ((entry = readdir(directory)) != NULL) {
        printf("%s\n", entry->d_name);
    }

    // Close the directory
    closedir(directory);

    return 0;
}

