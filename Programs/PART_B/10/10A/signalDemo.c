#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

void sigint_handler(int sig) {
    printf("\nReceived SIGINT (Ctrl+C). Exiting...\n");
    exit(0);
}

void sigalrm_handler(int sig) {
    printf("\nReceived SIGALRM (Alarm). Alarm triggered!\n");
}

int main() {
    // Registering signal handlers
    if (signal(SIGINT, sigint_handler) == SIG_ERR) {
        perror("Unable to register SIGINT handler");
        return 1;
    }

    if (signal(SIGALRM, sigalrm_handler) == SIG_ERR) {
        perror("Unable to register SIGALRM handler");
        return 1;
    }

    // Set an alarm to trigger after 7 seconds
    alarm(7);

    printf("Waiting for signals...\n");

    // Loop infinitely
    while(1) {
        // Do nothing, just keep waiting for signals
        sleep(1);
    }

    return 0;
}

