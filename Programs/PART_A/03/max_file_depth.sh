max_file_depth() {
    if [ $# -ne 1 ]; then
        echo "Usage: max_file_depth <directory>"
        return 1
    fi

    local directory="$1"
    local max_depth=0

    if [ ! -d "$directory" ]; then
        echo "Error: '$directory' is not a valid directory."
        return 1
    fi

    # Function to calculate maximum depth
    calculate_max_depth() {
        local current_dir="$1"
        local current_depth="$2"

        # Update max depth if the current depth is greater
        if [ "$current_depth" -gt "$max_depth" ]; then
            max_depth="$current_depth"
        fi

        # Traverse subdirectories recursively
        local file
        while IFS= read -r -d '' file; do
            if [ -d "$file" ]; then
                calculate_max_depth "$file" "$((current_depth + 1))"
            fi
        done < <(find "$current_dir" -mindepth 1 -maxdepth 1 -type d -print0) #find only subdirectories in the current directory
    }

    # Start calculating maximum depth from the root directory
    calculate_max_depth "$directory" 0

	echo
    echo "Maximum depth of any file in '$directory' directory is: $max_depth"
    echo
}

max_file_depth $1
